#!/usr/bin/env bash

for i in routes/routes_split_*.route; do
    ./kwp -z basechars/full.base keymaps/en-us.keymap $i -o output-$i.txt
    echo done $i
done
