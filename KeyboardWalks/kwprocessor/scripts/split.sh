#!/usr/bin/env bash

file=<large_route_file>
num_files=<split_parts>

total_lines=$(wc -l <${file})
((lines_per_file = (total_lines + num_files - 1) / num_files))

split --lines=${lines_per_file} ${file} -d file_output_ --additional-suffix=.route

echo "Total lines    = ${total_lines}"
echo "Lines per file = ${lines_per_file}"    
