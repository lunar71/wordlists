mywordlist.txt
==============

Compiled from the following SecLists wordlists, sorted and uniqued:

    websphere.txt weblogic.txt tomcat.txt swagger.txt sunas.txt SunAppServerGlassfish.fuzz.txt spring-boot.txt sap.txt ror.txt PHP.fuzz.txt oracle.txt 'Oracle EBS wordlist.txt' OracleAppServer.fuzz.txt Oracle9i.fuzz.txt LotusNotes.fuzz.txt Logins.fuzz.txt jrun.txt JRun.fuzz.txt Jenkins-Hudson.txt jboss.txt JavaServlets-Common.fuzz.txt IIS.fuzz.txt hyperion.txt Hyperion.fuzz.txt graphql.txt frontpage.txt Frontpage.fuzz.txt FatwireCMS.fuzz.txt domino-endpoints-coldfusion39.txt domino-dirs-coldfusion39.txt confluence-administration.txt coldfusion.txt axis.txt apache.txt ApacheTomcat.fuzz.txt AdobeXML.fuzz.txt AdobeCQ-AEM.txt

cgi-wordlist.txt
================

Compiled from the following SecLists wordlists, sorted and uniqued:

    CGI-HTTP-POST.fuzz.txt CGI-HTTP-POST-Windows.fuzz.txt CGI-Microsoft.fuzz.txt CGIs.txt CGI-XPlatform.fuzz.txt

